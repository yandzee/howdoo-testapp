const path = require('path');

const srcPath = function(p) {
  return path.join(__dirname, 'src', p);
};

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  output: {
    filename: 'app.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      '@js': srcPath('.'),
    },
  },
  module: {
    rules: [
      {
        test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
          {
              loader: 'ts-loader'
          }
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'source-map-loader'
      },
    ]
  },
  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM'
  },
};
