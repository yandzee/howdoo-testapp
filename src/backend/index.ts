import { Dictionary } from '@js/misc'
import { IProductForm, IProduct } from '@js/domain/product'
import { IWarehouseForm, IWarehouse } from '@js/domain/warehouse'

// distribution is an object of following shape:
// {
//   [productId]: {
//     [warehouseId]: number; // number is amount of productId on this warehouse
//   }
// }

export interface Database {
  products: Array<IProduct>;
  warehouses: Array<IWarehouse>;
  distribution: Dictionary<Dictionary<number> | undefined>;
}

class LocalBackend { // ><
  static readonly dbName = 'howdoo-db';
  private cachedDatabase: Database;

  constructor() {
    this.cachedDatabase = this.getCurrentData();
  }

  public createProduct(product: IProductForm): number {
    const pid = this.nextProductId();

    this.cachedDatabase.products.push({
      id: pid,
      name: product.name,
      amount: product.amount,
    });

    this.cachedDatabase.distribution[pid] = product.distribution;

    this.saveData(this.cachedDatabase);
    return pid;
  }

  public updateProduct(product: IProductForm) {
    const p = this.cachedDatabase.products.find((pp: IProduct) => {
      return pp.id === product.id;
    });

    if (p == null) return;

    p.id = product.id;
    p.name = product.name;
    p.amount = product.amount;

    this.cachedDatabase.distribution[p.id] = product.distribution;
    this.saveData(this.cachedDatabase);
  }

  private nextProductId(): number {
    const productIds = this.cachedDatabase.products.map(p => p.id);

    return productIds.length === 0 ? 1 : Math.max(...productIds) + 1;
  }

  private nextWarehouseId(): number {
    const whIds = this.cachedDatabase.warehouses.map(w => w.id);

    return whIds.length === 0 ? 1 : Math.max(...whIds) + 1;
  }

  public createWarehouse(wh: IWarehouseForm): number {
    const whId = this.nextWarehouseId();

    this.cachedDatabase.warehouses.push({
      id: whId,
      name: wh.name,
    });

    this.updateWarehouseDistribution(wh);
    this.saveData(this.cachedDatabase);
    return whId;
  }

  public updateWarehouse(wh: IWarehouseForm) {
    console.log('in Backend::updateWarehouse', wh);
    const w = this.cachedDatabase.warehouses.find((swh: IWarehouse) => {
      return swh.id === wh.id;
    });

    if (w == null) return;

    w.name = wh.name;
    this.updateWarehouseDistribution(wh);
  }

  private updateWarehouseDistribution(wh: IWarehouseForm) {
    Object.keys(wh.distribution).forEach((key: string) => {
      const productId = parseInt(key);
      const productDist = this.cachedDatabase.distribution[productId] || {};

      productDist[wh.id] = wh.distribution[productId];

      this.cachedDatabase.distribution[productId] = productDist;
    });

    this.saveData(this.cachedDatabase);
  }

  public getCurrentData(): Database {
    const raw: string | null = localStorage.getItem(LocalBackend.dbName);
    if (raw != null && raw.length !== 0) {
      return JSON.parse(raw) as Database;
    }

    return {
      products: [],
      warehouses: [],
      distribution: {},
    };
  }

  private saveData(database: Database) {
    const serialized = JSON.stringify(database, null, 2);

    localStorage.setItem(LocalBackend.dbName, serialized);
  }
}

export default new LocalBackend();
