import CoreAPI from './core'
import ApiResponse from './api-response'

export { ApiResponse };

export default {
  core: CoreAPI.new()
} as { core: CoreAPI }
