export interface IResponseObject<DataType> {
  data: DataType;
  error: boolean;
}

export default class ApiResponse<DataType> {
  private response: IResponseObject<DataType>; // Just mock, in fact, it is AxiosResponse<T>

  constructor(resp: IResponseObject<DataType>) {
    this.response = resp;
  }

  public static mockedSuccess<D>(data: D): ApiResponse<D> {
    return new ApiResponse<D>({ data, error: false });
  }

  public get data(): DataType {
    return this.response.data;
  }

  public get ok(): boolean {
    return true;
  }

  public get isError(): boolean {
    return false;
  }
}
