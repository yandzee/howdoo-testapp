import ApiResponse from './api-response'

import { IProductForm } from '@js/domain/product'
import { IWarehouseForm } from '@js/domain/warehouse'

import Backend, { Database } from '@js/backend'

export default class CoreAPI {
  public static new(): CoreAPI {
    return new CoreAPI();
  }

  public async fetchAccounting(): Promise<ApiResponse<Database>> {
    const data = Backend.getCurrentData();
    const response = ApiResponse.mockedSuccess(data);

    return Promise.resolve(response);
  }

  public async createProduct(product: IProductForm): Promise<ApiResponse<number>> {
    const productId = Backend.createProduct(product);
    const response = ApiResponse.mockedSuccess(productId);

    return Promise.resolve(response);
  }

  public async updateProduct(product: IProductForm): Promise<ApiResponse<void>> {
    Backend.updateProduct(product);
    const response = ApiResponse.mockedSuccess(void 0);

    return Promise.resolve(response);
  }

  public async createWarehouse(wh: IWarehouseForm): Promise<ApiResponse<number>> {
    const whId = Backend.createWarehouse(wh);
    const response = ApiResponse.mockedSuccess(whId);

    return Promise.resolve(response);
  }

  public async updateWarehouse(wh: IWarehouseForm): Promise<ApiResponse<void>> {
    Backend.updateWarehouse(wh);
    const response = ApiResponse.mockedSuccess(void 0);

    return Promise.resolve(response);
  }
}
