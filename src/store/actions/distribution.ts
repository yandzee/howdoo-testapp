import { AnyAction } from 'redux'

import { Dictionary } from '@js/misc'
import { DistributionState } from '@js/store/reducers/distribution'

export const SET_DISTRIBUTION = 'SET_DISTRIBUTION';
export const UPDATE_PRODUCT_DISTRIBUTION = 'UPDATE_PRODUCT_DISTRIBUTION'
export const UPDATE_WAREHOUSE_DISTRIBUTION = 'UPDATE_WAREHOUSE_DISTRIBUTION'

export interface SetAction extends AnyAction {
  distribution: Dictionary<Dictionary<number>>;
}

export interface UpdateItemDistribution extends AnyAction {
  distribution: Dictionary<number>;
}

export interface UpdateProductDistribution extends UpdateItemDistribution {
  productId: number;
}

export interface UpdateWarehouseDistribution extends UpdateItemDistribution {
  warehouseId: number;
}

export type DistAction =
  | SetAction
  | UpdateProductDistribution
  | UpdateWarehouseDistribution;

export const set = (distribution: DistributionState): SetAction => {
  return { type: SET_DISTRIBUTION, distribution };
};

export const updateProductDistribution = (
  productId: number,
  distribution: Dictionary<number>,
): UpdateProductDistribution => {
  return {
    type: UPDATE_PRODUCT_DISTRIBUTION,
    productId,
    distribution,
  };
};

export const updateWarehouseDistribution = (
  warehouseId: number,
  distribution: Dictionary<number>,
): UpdateWarehouseDistribution => {
  return {
    type: UPDATE_WAREHOUSE_DISTRIBUTION,
    warehouseId,
    distribution,
  };
};
