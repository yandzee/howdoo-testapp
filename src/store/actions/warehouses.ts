import { AnyAction } from 'redux'
import { IWarehouse, IWarehouseForm } from '@js/domain/warehouse'

export const SET_WAREHOUSES = 'SET_WAREHOUSES';
export const UPDATE_WAREHOUSE = 'UPDATE_WAREHOUSE';

export interface SetAction extends AnyAction {
  warehouses: Array<IWarehouse>;
};

export interface UpdateAction extends AnyAction {
  warehouseId?: number;
  warehouseData: IWarehouseForm;
}

export type WarehouseAction =
  | SetAction
  | UpdateAction;

export const set = (warehouses: Array<IWarehouse>): SetAction => {
  return { type: SET_WAREHOUSES, warehouses };
};

export const update = (
  warehouseId: number | undefined,
  warehouseData: IWarehouseForm
): UpdateAction => {
  return { type: UPDATE_WAREHOUSE, warehouseId, warehouseData };
}
