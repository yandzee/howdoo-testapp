import { AnyAction } from 'redux'

import { IProduct, IProductForm } from '@js/domain/product'

export const SET_PRODUCTS = 'SET_PRODUCTS';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';

export interface SetAction extends AnyAction {
  products: Array<IProduct>;
}

export interface UpdateAction extends AnyAction {
  productId?: number;
  productData: IProductForm;
}

export type ProductAction =
  | SetAction
  | UpdateAction;

export const set = (products: Array<IProduct>): SetAction => {
  return { type: SET_PRODUCTS, products };
};

export const update = (
  productId: number | undefined,
  productData: IProductForm,
): UpdateAction => {
  return { type: UPDATE_PRODUCT, productId, productData };
}
