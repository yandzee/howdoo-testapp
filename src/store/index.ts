import { createStore } from 'redux'
import config from '@js/config'

import appReducer from './reducers'
import { WarehousesState } from './reducers/warehouses'
import { ProductsState } from './reducers/products'
import { DistributionState } from './reducers/distribution'

import { Dictionary } from '@js/misc'

export { WarehousesState, ProductsState, DistributionState };

export interface Store {
  warehouses: WarehousesState;
  products: ProductsState;
  distribution: DistributionState;
}

const devTools = (
  window.__REDUX_DEVTOOLS_EXTENSION__ &&
  window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(appReducer, config.debug ? devTools : undefined);
export default store;
