import { IWarehouse } from '@js/domain/warehouse'
import {
  SET_WAREHOUSES,
  UPDATE_WAREHOUSE,

  WarehouseAction,
  SetAction,
  UpdateAction,
} from '../actions/warehouses'

export type WarehousesState = Array<IWarehouse>;

export const warehouses = (state: WarehousesState = [], action: WarehouseAction) => {
  const acts = {
    [SET_WAREHOUSES]: setWarehouses,
    [UPDATE_WAREHOUSE]: updateWarehouse,
  } as any;

  return acts[action.type] ? acts[action.type](state, action) : state;
};

const setWarehouses = (state: WarehousesState, action: SetAction) => {
  return action.warehouses;
}

const updateWarehouse = (state: WarehousesState, action: UpdateAction) => {
  console.log('in updateWarehouse: ', state, action);

  if (action.warehouseId !== action.warehouseData.id) {
    return state.concat({
      id: action.warehouseId,
      name: action.warehouseData.name,
    });
  }

  return state.map((wh: IWarehouse) => {
    if (wh.id !== action.warehouseId) return wh;

    return {
      id: action.warehouseId,
      name: action.warehouseData.name,
    };
  });
};

export default warehouses;
