import { combineReducers } from 'redux'

import warehouses from './warehouses'
import products from './products'
import distribution from './distribution'

const appReducer = combineReducers({
  warehouses,
  products,
  distribution,
});

export default appReducer;
