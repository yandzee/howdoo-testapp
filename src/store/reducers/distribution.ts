import { AnyAction } from 'redux'

import {
  SET_DISTRIBUTION,
  UPDATE_PRODUCT_DISTRIBUTION,
  UPDATE_WAREHOUSE_DISTRIBUTION,

  DistAction,
  SetAction,
  UpdateProductDistribution,
  UpdateWarehouseDistribution,
} from '../actions/distribution'

import { Dictionary } from '@js/misc'

export type DistributionState = Dictionary<Dictionary<number> | undefined>;

export const distribution = (state: DistributionState = {}, action: DistAction) => {
  const acts = {
    [SET_DISTRIBUTION]: setDistribution,
    [UPDATE_PRODUCT_DISTRIBUTION]: updateProductDistribution,
    [UPDATE_WAREHOUSE_DISTRIBUTION]: updateWarehouseDistribution,
  } as any;

  return acts[action.type] ? acts[action.type](state, action) : state;
};

const setDistribution = (state: DistributionState, action: SetAction) => {
  return action.distribution;
};

const updateProductDistribution = (
  state: DistributionState,
  action: UpdateProductDistribution,
) => {
  const productDist = state[action.productId];
  const newProductDist = Object.assign({}, productDist, action.distribution);

  return Object.assign({}, state, {
    [action.productId]: newProductDist
  })
};

const updateWarehouseDistribution = (
  state: DistributionState,
  action: UpdateWarehouseDistribution,
) => {
  const newDistribution = Object.assign({}, state);

  Object.keys(action.distribution).forEach((key: string) => {
    const productId = parseInt(key);
    const productDist = Object.assign({}, newDistribution[productId]);

    productDist[action.warehouseId] = action.distribution[productId];
    newDistribution[productId] = productDist;
  });

  return newDistribution;
};

export default distribution;
