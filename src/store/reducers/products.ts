import { IProduct } from '@js/domain/product'
import {
  SET_PRODUCTS,
  UPDATE_PRODUCT,

  ProductAction,
  SetAction,
  UpdateAction,
} from '../actions/products'

export type ProductsState = Array<IProduct>;

export const products = (state: ProductsState = [], action: ProductAction) => {
  const acts = {
    [SET_PRODUCTS]: setProducts,
    [UPDATE_PRODUCT]: updateProduct,
  } as any;

  return acts[action.type] ? acts[action.type](state, action) : state;
};

const setProducts = (state: ProductsState, action: SetAction) => {
  return action.products;
};

const updateProduct = (state: ProductsState = [], action: UpdateAction) => {
  if (action.productId !== action.productData.id) {
    return state.concat({
      id: action.productId,
      name: action.productData.name,
      amount: action.productData.amount,
    });
  }

  return state.map((product: IProduct) => {
    if (product.id !== action.productId) {
      return product;
    }

    return {
      id: product.id,
      name: action.productData.name,
      amount: action.productData.amount,
    };
  });
};

export default products;
