import { Store } from '@js/store'
import { IWarehouseForm } from '@js/domain/warehouse'

export const getWarehouseForm = (state: Store, whId: number): IWarehouseForm => {
  const whData = state.warehouses.find(w => w.id === whId);
  const distribution: any = {};

  Object.keys(state.distribution).forEach((productId: string) => {
    const productDist = state.distribution[productId];

    distribution[productId] = productDist[whId] || 0;
  });

  return { ...whData, distribution };
}
