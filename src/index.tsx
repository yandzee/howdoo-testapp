import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store, { Store } from './store';
import * as whActions from './store/actions/warehouses';
import * as pActions from './store/actions/products';
import * as dActions from './store/actions/distribution';

import api, { ApiResponse } from './api';

import './styles/index.scss';
import { StorehouseAccounting } from './components/StorehouseAccounting';

const render = () => {
	ReactDOM.render(
		<Provider store={store}>
			<StorehouseAccounting />
		</Provider>,
		document.getElementById('app')
	);
};

api.core.fetchAccounting().then((res: ApiResponse<Store>) => {
	if (!res.ok) {
		// TODO: show UI notification
		throw new Error('failure case is not covered in test app ><');
	}

	const warehouses = res.data.warehouses;
	const products = res.data.products;
	const distribution = res.data.distribution;

	store.dispatch(whActions.set(warehouses));
	store.dispatch(pActions.set(products));
	store.dispatch(dActions.set(distribution));

	render();
});
