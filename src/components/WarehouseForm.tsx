import * as React from 'react';
import {
  Link,
  RouteComponentProps,
  Switch,
  Route,
} from 'react-router-dom'
import { connect } from 'react-redux'

import AmountWidget from './AmountWidget'

import { Store } from '@js/store'
import {
  IWarehouse,
  IWarehouseForm,
  newEmptyWarehouseForm,
} from '@js/domain/warehouse'

import { IProduct } from '@js/domain/product'
import { Dictionary } from '@js/misc'

import { DistributionState, WarehousesState, ProductsState } from '@js/store'

export enum FormMode {
  EDIT, CREATE
}

export interface FormState {
  mode: FormMode;
  form: IWarehouseForm;
}

interface FormProps extends RouteComponentProps<any> {
  data?: IWarehouseForm;
  warehouses: WarehousesState;
  products: ProductsState;
  distribution: DistributionState;
  onSave: (mode: FormMode, data: IWarehouseForm) => void;
}

class WarehouseForm extends React.Component<FormProps, FormState> {
  constructor(props: FormProps) {
    super(props);

    this.state = {
      mode: this.props.data == null ? FormMode.CREATE : FormMode.EDIT,
      form: this.props.data || newEmptyWarehouseForm(),
    };
  }

  handleNameInput(event: React.FormEvent<HTMLInputElement>) {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    const name = target.value;

    this.setState({
      form: { ...this.state.form, name },
    });
  }

  render() {
    return (
      <div className="products">
        <div className="new form">
          <div className="headline">
            <div className="caption">New warehouse</div>
            <div className="actions">
              <div className="btn red" onClick={
                () => this.props.history.push('/warehouses')
              }>Cancel</div>
            </div>
          </div>
          <div className="field form">
            <div className="row general">
              <div className="field group">
                <div className="caption">Name</div>
                <input type="text" value={this.state.form.name} onChange={
                  (e) => this.handleNameInput(e)
                }/>
              </div>

              <div className="field group actions">
                <div onClick={() => this.onSave() } className={
                  `btn red ${this.saveIsInactive() ? 'inactive': ''}`
                }>Save</div>
              </div>
            </div>
            <div className="distribution">
              { this.renderProductDistribution() }
            </div>
          </div>
        </div>
      </div>
    );
  }

  saveIsInactive(): boolean {
    return false;
  }

  renderProductDistribution() {
    if (this.props.products.length === 0) {
      return <div className="empty">No products.</div>
    }

    if (this.props.products.length === 0) {
      return <div className="empty">
        No products in database.
      </div>
    }

    return (
      <div className="distributions">
        <div className="caption">
          Select products to store in this warehouse.
        </div>

        {this.props.products.map((p: IProduct) => {
          return (
            <div className="warehouse-distribution" key={p.id}>
              <div className="headline">
                <div className="id">id: {p.id}</div>
                <div className="name">{p.name}</div>
              </div>
              <div className="actions">
                {<AmountWidget
                  lbutton={this.lessIsAvailableForProduct(p.id)}
                  rbutton={this.moreIsAvailableForProduct(p.id)}
                  value={this.amountForProduct(p.id)}
                  onChange={(diff: number) => {
                    this.changeAmountForProduct(p.id, diff);
                  }}
                />}
              </div>
            </div>
          );
        })}
      </div>
    )
  }

  lessIsAvailableForProduct(productId: number): boolean {
    return !!this.state.form.distribution[productId];
  }

  moreIsAvailableForProduct(productId: number): boolean {
    const [total, _] = this.productAmounts(productId);
    const inCurrentWarehouse = (this.state.form.distribution[productId] || 0);
    const inOtherWarehouses = this.amountInOtherWarehouses(productId);

    return total - inOtherWarehouses - inCurrentWarehouse > 0;
  }

  productAmounts(productId: number): [number, number] {
    const product = this.props.products.find(p => p.id === productId);
    const productDist = this.props.distribution[productId];
    const whIds = Object.keys(productDist);

    const distributedAmount = whIds.reduce((acc: number, owhId: string) => {
      return acc + this.state.form.distribution[owhId]
    }, 0);

    return [product.amount, distributedAmount];
  }

  amountInOtherWarehouses(productId: number): number {
    const currentWhId = this.props.data.id;
    const productDist = this.props.distribution[productId];
    const whIds = Object.keys(productDist);

    return whIds.reduce((acc: number, key: string) => {
      const owhId = parseInt(key);
      if (owhId === currentWhId) return acc;

      return acc + (this.props.distribution[productId][owhId] || 0);
    }, 0);
  }

  amountForProduct(productId: number): number {
    return this.state.form.distribution[productId] || 0;
  }

  changeAmountForProduct(productId: number, diff: number) {
    const newAmount = (this.state.form.distribution[productId] || 0) + diff;
    const distribution = { ...this.state.form.distribution,
      [productId]: newAmount
    };

    this.setState({
      form: { ...this.state.form, distribution },
    })
  }

  onSave() {
    this.props.onSave(this.state.mode, this.state.form);
  }
}

const mapStateToProps = (state: Store) => {
  return {
    warehouses: state.warehouses,
    products: state.products,
    distribution: state.distribution,
  }
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(WarehouseForm);
