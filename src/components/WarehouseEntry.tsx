import * as React from 'react';
import { connect } from 'react-redux'

import { IWarehouse, IWarehouseForm } from '@js/domain/warehouse'

import '@js/styles/warehouse-entry.scss'

interface Props {
  data: IWarehouseForm;
  onEdit: Function;
}

class WarehouseEntry extends React.Component<Props, {}> {
  render() {
    return (
      <div className="warehouse entry">
        <div className="headline">
          <div className="captions">
            <span className="id">id: {this.props.data.id}</span>
            <span>{this.props.data.name}</span>
          </div>
          <div className="props">
            <span>Items: {this.numItems}</span>
          </div>
          <div className="actions">
            <span onClick={() => this.props.onEdit(this.props.data)}>Edit</span>
          </div>
        </div>
      </div>
    );
  }

  get numItems() {
    const distribution = this.props.data.distribution;
    let num = 0;

    Object.keys(distribution).forEach((productId: string) => {
      if (distribution[productId] === 0) return;

      num += 1;
    });

    return num;
  }
}

export default WarehouseEntry;
