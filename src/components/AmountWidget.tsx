import * as React from 'react';

import '@js/styles/widgets/amount.scss'

interface WidgetProps {
  value: number;
  lbutton: boolean;
  rbutton: boolean;
  onChange: (diff: number) => void;
}

interface WidgetState {
}

class AmountWidget extends React.Component<WidgetProps, WidgetState> {
  render() {
    return (
      <div className="amount widget">
        <div className={
          `button decrease ${!this.props.lbutton ? 'disabled' : ''}`
        } onClick={ () => this.props.onChange(-1)}>-</div>
        <div className="counter">{ this.props.value }</div>
        <div className={
          `button increase ${!this.props.rbutton ? 'disabled' : ''}`
        } onClick={ () => this.props.onChange(+1)}>+</div>
      </div>
    );
  }
}

export default AmountWidget
