import * as React from 'react';
import {
  Link,
  RouteComponentProps,
  Switch,
  Route,
} from 'react-router-dom'
import { connect } from 'react-redux'

import { Store, DistributionState } from '@js/store'
import { IWarehouse, IWarehouseForm } from '@js/domain/warehouse'

import { getWarehouseForm } from '@js/store/getters'
import { update as updateWarehouse } from '@js/store/actions/warehouses'
import { updateWarehouseDistribution } from '@js/store/actions/distribution'

import api, { ApiResponse } from '@js/api'

import WarehouseList from './WarehouseList'
import WarehouseForm, { FormMode as WarehouseFormMode } from './WarehouseForm'

import '@js/styles/warehouses.scss'

interface Props extends RouteComponentProps<any> {
  warehouses: Array<IWarehouse>;
  distribution: DistributionState;
  updateWarehouse: Function;
  updateWarehouseDistribution: Function;
  getWarehouseForm: Function;
}

class Warehouses extends React.Component<Props, {}> {
  render() {
    return (
      <Switch>
        <Route exact path={this.props.match.url} component={WarehouseList} />
        <Route path={`${this.props.match.url}/new`} render={(props) => {
          return <WarehouseForm {...props} onSave={
            (...args) => this.onSave(...args)
          } />
        }} />
        <Route path={`${this.props.match.url}/edit/:warehouseId`} render={
          (props) => {
            const warehouseId = parseInt(props.match.params.warehouseId);
            const warehouseData = this.props.getWarehouseForm(warehouseId);

            return <WarehouseForm {...props} data={warehouseData} onSave={
              (...args) => this.onSave(...args)
            } />
          }
        } />
      </Switch>
    );
  }

  onSave(mode: WarehouseFormMode, formData: IWarehouseForm) {
    console.log('in warehouse onSave: ', WarehouseFormMode[mode], formData);

    if (mode === WarehouseFormMode.CREATE) {
      this.createNewWarehouse(formData);
    } else {
      this.updateWarehouse(formData);
    }

    this.props.history.push('/warehouses');
  }

  createNewWarehouse(warehouseData: IWarehouseForm) {
    api.core.createWarehouse(warehouseData).then((res: ApiResponse<number>) => {
      const warehouseId = res.data;

      this.props.updateWarehouse(warehouseId, warehouseData);
    });
  }

  updateWarehouse(warehouseData: IWarehouseForm) {
    api.core.updateWarehouse(warehouseData).then((res: ApiResponse<void>) => {
      this.props.updateWarehouse(warehouseData.id, warehouseData);
      this.props.updateWarehouseDistribution(
        warehouseData.id,
        warehouseData.distribution
      );
    });
  }
}

const mapStateToProps = (state: Store) => {
  return {
    warehouses: state.warehouses,
    distribution: state.distribution,
    getWarehouseForm: (whId: number) => getWarehouseForm(state, whId),
  }
};

const mapDispatchToProps = {
  updateWarehouse,
  updateWarehouseDistribution,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Warehouses);
