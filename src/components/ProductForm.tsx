import * as React from 'react';
import {
  Link,
  RouteComponentProps,
  Switch,
  Route,
} from 'react-router-dom'
import { connect } from 'react-redux'

import AmountWidget from './AmountWidget'

import { Store } from '@js/store'
import { IProduct, IProductForm, newEmptyProductForm } from '@js/domain/product'
import { IWarehouse } from '@js/domain/warehouse'
import { Dictionary } from '@js/misc'

export enum FormMode {
  EDIT, CREATE
}

export interface FormState {
  mode: FormMode;
  form: IProductForm;
}

interface FormProps extends RouteComponentProps<any> {
  data?: IProductForm;
  warehouses: Array<IWarehouse>;
  onSave: (mode: FormMode, data: IProductForm) => void;
}

class ProductForm extends React.Component<FormProps, FormState> {
  constructor(props: FormProps) {
    super(props);

    this.state = {
      mode: this.props.data == null ? FormMode.CREATE : FormMode.EDIT,
      form: this.props.data || newEmptyProductForm(),
    };
  }

  handleNameInput(event: React.FormEvent<HTMLInputElement>) {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    const name = target.value;

    this.setState({
      form: { ...this.state.form, name },
    });
  }

  handleAmountChange(diff: number) {
    const amount = this.state.form.amount + diff;

    this.setState({
      form: { ...this.state.form, amount },
    });
  }

  render() {
    return (
      <div className="products">
        <div className="new form">
          <div className="headline">
            <div className="caption">New product</div>
            <div className="actions">
              <div className="btn red" onClick={
                () => this.props.history.push('/products')
              }>Cancel</div>
            </div>
          </div>
          <div className="field form">
            <div className="row general">
              <div className="field group">
                <div className="caption">Name</div>
                <input type="text" value={this.state.form.name} onChange={
                  (e) => this.handleNameInput(e)
                }/>
              </div>

              <div className="field group">
                <div className="caption">Amount</div>
                <AmountWidget
                  lbutton={this.lessIsAvailable()}
                  rbutton={true} value={this.state.form.amount}
                  onChange={(diff: number) => this.handleAmountChange(diff)}
                />
              </div>

              <div className="field group actions">
                <div onClick={() => this.onSave() } className={
                  `btn red ${this.saveIsInactive() ? 'inactive': ''}`
                }>Save</div>
              </div>
            </div>
            <div className="distribution">
              { this.renderProductDistribution() }
            </div>
          </div>
        </div>
      </div>
    );
  }

  saveIsInactive(): boolean {
    return false;
  }

  lessIsAvailable(): boolean {
    // return false if current amount is already distributed
    const total = this.state.form.amount;
    return total !== 0 && total > this.distributedAmount();
  }

  renderProductDistribution() {
    if (this.props.warehouses.length === 0) {
      return <div className="empty">No warehouses.</div>
    }

    if (this.state.form.amount === 0) {
      return <div className="empty">
        No units available for distribution.
      </div>
    }

    return (
      <div className="distributions">
        <div className="caption">
          Distribute this product among warehouses ({
            this.state.form.amount - this.distributedAmount()
          } left):
        </div>

        {this.props.warehouses.map((wh: IWarehouse) => {
          return (
            <div className="warehouse-distribution" key={wh.id}>
              <div className="headline">
                <div className="id">id: {wh.id}</div>
                <div className="name">{wh.name}</div>
              </div>
              <div className="actions">
                {<AmountWidget
                  lbutton={this.lessIsAvailableForWarehouse(wh.id)}
                  rbutton={this.moreIsAvailableForWarehouse(wh.id)}
                  value={this.amountForWarehouse(wh.id)}
                  onChange={(diff: number) => {
                    this.changeAmountForWarehouse(wh.id, diff);
                  }}
                />}
              </div>
            </div>
          );
        })}
      </div>
    )
  }

  lessIsAvailableForWarehouse(whId: number): boolean {
    return !!this.state.form.distribution[whId];
  }

  moreIsAvailableForWarehouse(whId: number): boolean {
    const total = this.state.form.amount;

    return this.distributedAmount() < total;
  }

  distributedAmount(): number {
    const whIds = Object.keys(this.state.form.distribution);

    return whIds.reduce((acc: number, owhId: string) => {
      return acc + this.state.form.distribution[owhId]
    }, 0);
  }

  amountForWarehouse(whId: number): number {
    return this.state.form.distribution[whId] || 0;
  }

  changeAmountForWarehouse(whId: number, diff: number) {
    const newAmount = (this.state.form.distribution[whId] || 0) + diff;
    const distribution = { ...this.state.form.distribution,
      [whId]: newAmount
    };

    this.setState({
      form: { ...this.state.form, distribution },
    })
  }

  onSave() {
    this.props.onSave(this.state.mode, this.state.form);
  }
}

const mapStateToProps = (state: Store) => {
  return {
    warehouses: state.warehouses,
  }
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductForm);
