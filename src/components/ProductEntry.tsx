import * as React from 'react';
import { connect } from 'react-redux'

import { IProductForm } from '@js/domain/product'

import '@js/styles/product-entry.scss'

interface EntryProps {
  data: IProductForm;
  onEdit: Function;
}

class ProductEntry extends React.Component<EntryProps, {}> {
  render() {
    return (
      <div className="product entry">
        <div className="headline">
          <div className="captions">
            <span className="id">id: {this.props.data.id}</span>
            <span>{this.props.data.name}</span>
          </div>
          <div className="props">
            <span>Amount: {this.props.data.amount}</span>
            <span>Non-distributed: {this.nonDistributedAmount()}</span>
            <span>Distributed to {this.numWarehouses()} warehouses</span>
          </div>
          <div className="actions">
            <span onClick={() => this.props.onEdit(this.props.data)}>Edit</span>
          </div>
        </div>
      </div>
    );
  }

  numWarehouses(): number {
    return Object.keys(this.props.data.distribution).length;
  }

  nonDistributedAmount(): number {
    const keys = Object.keys(this.props.data.distribution);
    const ndistributed = keys.reduce((acc: number, key: string) => {
      return acc + this.props.data.distribution[key]
    }, 0);

    return this.props.data.amount - ndistributed;
  }
}

export default ProductEntry;
