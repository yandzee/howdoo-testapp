import * as React from 'react';
import {
  Link,
  RouteComponentProps,
  Switch,
  Route,
} from 'react-router-dom'
import { connect } from 'react-redux'

import { Store } from '@js/store'
import { DistributionState } from '@js/store/reducers/distribution'
import { IProduct, IProductForm } from '@js/domain/product'
import { Dictionary } from '@js/misc'
import api, { ApiResponse } from '@js/api'

import { update as updateProduct } from '@js/store/actions/products'
import { updateProductDistribution } from '@js/store/actions/distribution'

import ProductForm, { FormMode as ProductFormMode } from './ProductForm'
import ProductsList from './ProductsList'

import '@js/styles/products.scss'

interface ProductsProps extends RouteComponentProps<any> {
  products: Array<IProduct>;
  distribution: DistributionState;
  updateProduct: Function;
  updateProductDistribution: Function;
}

class Products extends React.Component<ProductsProps, {}> {
  render() {
    return (
      <Switch>
        <Route exact path={this.props.match.url} component={ProductsList} />
        <Route path={`${this.props.match.url}/new`} render={(props) => {
          return <ProductForm {...props} onSave={
            (...args) => this.onSave(...args)
          } />
        }} />
        <Route path={`${this.props.match.url}/edit/:productId`} render={
          (props) => {
            const productId = parseInt(props.match.params.productId);
            const productForm = this.collectEditProductForm(productId);

            console.log('collected form: ', productForm);

            return <ProductForm {...props} data={productForm} onSave={
              (...args) => this.onSave(...args)
            } />
          }
        } />
      </Switch>
    );
  }

  onSave(mode: ProductFormMode, formData: IProductForm) {
    console.log('on product save: ', ProductFormMode[mode], formData);

    if (mode === ProductFormMode.CREATE) {
      this.createNewProduct(formData);
    } else {
      this.updateProduct(formData);
    }

    this.props.history.push('/products');
  }

  collectEditProductForm(productId: number): IProductForm {
    const product = this.props.products.find(p => p.id === productId);
    const distribution = this.props.distribution[productId] || {};

    return { ...product, distribution };
  }

  createNewProduct(productData: IProductForm) {
    api.core.createProduct(productData).then((res: ApiResponse<number>) => {
      const productId: number = res.data;

      this.props.updateProduct(productId, productData);
    });
  }

  updateProduct(productData: IProductForm) {
    api.core.updateProduct(productData).then((res: ApiResponse<void>) => {
      this.props.updateProduct(productData.id, productData);
      this.props.updateProductDistribution(productData.id, productData.distribution);
    });
  }
}

const mapStateToProps = (state: Store) => {
  return {
    products: state.products,
    distribution: state.distribution,
  }
};

const mapDispatchToProps = {
  updateProduct,
  updateProductDistribution,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Products);
