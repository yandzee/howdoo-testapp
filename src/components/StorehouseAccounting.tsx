import * as React from 'react';
import {
  NavLink,
  HashRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import api from '../api'

import Warehouses from './Warehouses'
import Products from './Products'

export class StorehouseAccounting extends React.Component<{}, {}> {
  render() {
    return (
      <div className="storehouse">
        <div className="narrowed">
          <div className="caption">Storehouse Accounting</div>
        </div>

        <Router>
          <div className="menu">
            <div className="links">
              <NavLink to="/warehouses">Warehouses</NavLink>
              <NavLink to="/products">Products</NavLink>
            </div>
          </div>

          <Switch>
            <Route path="/warehouses" component={Warehouses} />
            <Route path="/products" component={Products} />
          </Switch>
        </Router>
      </div>
    );
  }
}
