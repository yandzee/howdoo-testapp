import * as React from 'react';
import { Link, RouteComponentProps, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { Store } from '@js/store';
import { getWarehouseForm } from '@js/store/getters'

import { IWarehouse, IWarehouseForm } from '@js/domain/warehouse';

import WarehouseEntry from './WarehouseEntry';

import '@js/styles/entities-list.scss'

interface ListProps extends RouteComponentProps<any> {
  warehouses: Array<IWarehouse>;
  getWarehouseForm: Function;
}

class StorehouseList extends React.Component<ListProps, {}> {
  render() {
    return (
      <div className="warehouses entities-list">
        <div className="menu">
          <div className="caption">{this.props.warehouses.length} warehouses</div>
          <div className="actions">
            <Link to={`${this.props.match.url}/new`}>
              <div className="btn red action">New warehouse</div>
            </Link>
          </div>
        </div>

        { this.renderWarehouseList() }
      </div>
    );
  }

  renderWarehouseList() {
    if (this.props.warehouses.length === 0) {
      return (
        <div className="empty placeholder">
          No warehouses in database. Create one using button above.
        </div>
      );
    }

    return (
      <div className="list">
        {this.props.warehouses.map((w: IWarehouse) => {
          const warehouseData = this.collectWarehouseData(w);

          return <WarehouseEntry key={w.id} data={warehouseData} onEdit={
            (w: IWarehouse) => this.openWarehouseEdit(w)
          }/>
        })}
      </div>
    );
  }

  openWarehouseEdit(p: IWarehouse) {
    this.props.history.push(`/warehouses/edit/${p.id}`);
  }

  collectWarehouseData(w: IWarehouse): IWarehouseForm {
    return this.props.getWarehouseForm(w.id);
  }
}

const mapStateToProps = (state: Store) => {
  return {
    warehouses: state.warehouses,
    getWarehouseForm: (whId: number) => getWarehouseForm(state, whId),
  }
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(StorehouseList);
