import * as React from 'react';
import { Link, RouteComponentProps, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { Store, DistributionState, ProductsState } from '@js/store';
import { IProduct, IProductForm } from '@js/domain/product';

import ProductEntry from './ProductEntry';

import '@js/styles/entities-list.scss'

interface ProductsProps extends RouteComponentProps<any> {
  products: ProductsState;
  distribution: DistributionState;
}

class ProductsList extends React.Component<ProductsProps, {}> {
  render() {
    return (
      <div className="products entities-list">
        <div className="menu">
          <div className="caption">0 products</div>
          <div className="actions">
            <Link to={`${this.props.match.url}/new`}>
              <div className="btn red action">New product</div>
            </Link>
          </div>
        </div>

        { this.renderProductList() }
      </div>
    );
  }

  renderProductList() {
    if (this.props.products.length === 0) {
      return (
        <div className="empty placeholder">
          No products in database. Create one using button above.
        </div>
      );
    }

    return (
      <div className="list">
      {this.props.products.map((p: IProduct) => {
        const productData = this.collectProductData(p);

        return <ProductEntry key={p.id} data={productData} onEdit={
          (p: IProduct) => this.openProductEdit(p)
        }/>
      })}
      </div>
    );
  }

  collectProductData(p: IProduct): IProductForm {
    const distribution = this.props.distribution[p.id] || {};

    return { ...p, distribution };
  }

  openProductEdit(p: IProduct) {
    this.props.history.push(`/products/edit/${p.id}`);
  }
}

const mapStateToProps = (state: Store) => {
  return {
    products: state.products,
    distribution: state.distribution,
  }
};

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductsList);
