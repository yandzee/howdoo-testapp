import { Dictionary } from '@js/misc'

export interface IProduct {
  id: number;
  name: string;
  amount: number;
}

export interface IProductForm extends IProduct {
  distribution: Dictionary<number>;
}

export const newEmptyProductForm = (): IProductForm => {
  return {
    id: 0,
    name: '',
    amount: 0,
    distribution: {},
  };
};
