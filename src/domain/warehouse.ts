import { Dictionary } from '@js/misc'

export interface IWarehouse {
  id: number;
  name: string;
}

export interface IWarehouseForm extends IWarehouse {
  distribution: Dictionary<number>;
}

export const newEmptyWarehouseForm = (): IWarehouseForm => {
  return {
    id: 0,
    name: '',
    distribution: {},
  };
}
